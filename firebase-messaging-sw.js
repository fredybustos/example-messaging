// importScripts('https://www.gstatic.com/firebasejs/7.17.1/firebase-app.js')
// importScripts('https://www.gstatic.com/firebasejs/7.17.1/firebase-messaging.js')

importScripts('https://www.gstatic.com/firebasejs/6.5.0/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/6.5.0/firebase-messaging.js')

firebase.initializeApp({
  apiKey: 'AIzaSyB-dLRhz37LfhRKujE5kgTeQ1o9zAA0NL4',
  authDomain: 'cloud-messaging-react.firebaseapp.com',
  databaseURL: 'https://cloud-messaging-react.firebaseio.com',
  projectId: 'cloud-messaging-react',
  storageBucket: 'cloud-messaging-react.appspot.com',
  messagingSenderId: '857614245093',
  appId: '1:857614245093:web:6bbd40d7135f604aee080c',
  measurementId: 'G-60PK10B0SM'
})

let messaging = null

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
if (firebase.messaging.isSupported()) {
  messaging = firebase.messaging()
}
const windowOptions = {
  type: 'window',
  includeUncontrolled: true
}

messaging.setBackgroundMessageHandler(function (payload) {
  console.log('[firebase-messaging-sw.js] message ', payload)

  const title = payload.data.title
  const options = {
    body: payload.data.body,
    icon: './favicon.ico',
    data: {
      url: payload.data.click_action
    }
  }

  const promiseChain = self.clients
    .matchAll(windowOptions)
    .then(windowClients => {
      console.log(windowClients)
      for (let i = 0; i < windowClients.length; i++) {
        const windowClient = windowClients[i]
        windowClient.postMessage(payload)
      }
    })
    .then(() => {
      return self.registration.showNotification(title, options)
    })

  return promiseChain
})

self.addEventListener('notificationclick', function (event) {
  console.log('Notification notificationclick triggered')
  event.notification.close()

  // Evita que se abra una ventana adcional al dar click en la notificación
  const promiseChain = clients.matchAll(windowOptions).then(windowClients => {
    let matchingClient = null

    for (let i = 0; i < windowClients.length; i++) {
      const windowClient = windowClients[i]
      if (windowClient.url === event.notification.data.url) {
        matchingClient = windowClient
        break
      }
    }

    if (matchingClient) {
      return matchingClient.focus()
    } else {
      return clients.openWindow(event.notification.data.url)
    }
  })

  event.waitUntil(promiseChain)
})
